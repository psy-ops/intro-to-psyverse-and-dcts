# Intro to Psyverse and DCTs

### Knowing What We're Talking About: Facilitating Decentralized, Unequivocal Publication and Reference to Psychological Construct Definitions and Operationalizations

The rendered article is available at https://psy-ops.gitlab.io/intro-to-psyverse-and-dcts as a website and at https://psy-ops.gitlab.io/intro-to-psyverse-and-dcts/peters--crutzen---2022---knowing-what-we-re-talking-about--decentralized-unequivocality---manuscript.pdf as a PDF document.

Note that the preprint is located at https://doi.org/10.31234/osf.io/8tpcv.
